import React, { Component} from 'react';
// import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import './App.css';
import { ShoppingCartOutlined } from '@ant-design/icons';
import Product from './views/Product/Product';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import ProductDetailPage from './views/ProductDetail.js';
import 'antd/dist/antd.css';
import { Layout } from 'antd';


const { Header, Content } = Layout;

function App() {
  return (
    <div className="App">
     <Router>
     <Header className="site-layout-background">SHOP CART ONLINE <ShoppingCartOutlined key="setting" /></Header>
        <Switch>
          <Route path="/" exact component={Product} />
          <Route path="/product/:productID" component={ProductDetailPage} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
