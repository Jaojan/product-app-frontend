import { takeEvery, put, call, take } from 'redux-saga/effects'
import { loadDataSuccess, loadDataStart, loadDataFail,selectedProduct } from './actions/actions'
import { loadDataApi } from './api'


export function* onLoadDataStartAsync() {
    try {
        const response = yield call(loadDataApi);
        yield put(loadDataSuccess(response));
    } catch (e) {
        console.log(e);
        yield put(loadDataFail(e))
    }
}

// export function* onLoadProductAsync(productID) {
//     try {
//         const response = yield call(loadProdcutByID(productID));
//         yield put(selectedProduct(response));
//     } catch (e) {
//         console.log(e);
//         yield put(loadDataFail(e))
//     }
// }


export function* onLoadData(){
    yield takeEvery("LOAD_DATA_START", onLoadDataStartAsync)
    // yield take("SELECTED_PRODUCT", onLoadProductAsync(productID))
}