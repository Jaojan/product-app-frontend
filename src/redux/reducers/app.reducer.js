const INITIAL_STATE = {
    loading: false,
    data: [],
    errors: null
}


const appReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case "LOAD_DATA_START":
            return {
                ...state,
                loading: true
            };

        case "LOAD_DATA_SUCCESS":
            return {
                ...state,
                loading: false,
                data: action.payload
            };

        case "LOAD_DATA_FAIL":
            return {
                ...state,
                loading: false,
                data: action.payload
            };
        // case "SELECTED_PRODUCT":
        //         return {
        //             ...state,
        //             loading: false,
        //             data: action.payload
        //         };    
        default:
            return state;
    }
}

// SELECTED_PRODUCT

export default appReducer