export const loadDataStart = () => ({
    type: "LOAD_DATA_START",
});

export const loadDataSuccess = (data) => ({
    type: "LOAD_DATA_SUCCESS",
    payload: data.products
});

export const loadDataFail = (error) => ({
    type: "LOAD_DATA_FAIL",
    payload: error
})

// export const selectedProduct = (data) => {
//     return {
//       type: "SELECTED_PRODUCT",
//       payload: data.product,
//     };
//   };