export const loadDataApi = async () => {
    const response = await fetch("http://localhost:4000/product");
    const data = await response.json();
    return data;

}

export const loadProdcutByID = async (productID) => {
    const response = await fetch(`http://localhost:4000/product/${productID}`);
    const data = await response.json();
    console.log(data);
    return data;

}