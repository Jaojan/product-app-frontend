import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleWare from 'redux-saga'
import logger from 'redux-logger'
import rootReducer from './reducers';
import {onLoadData} from './sagas'

const sagaMiddleWare = createSagaMiddleWare();
const middlewares = [logger, sagaMiddleWare];

export const store = createStore(rootReducer, applyMiddleware(...middlewares))

sagaMiddleWare.run(onLoadData)

export default store;