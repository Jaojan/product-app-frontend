import React from 'react'
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import { Button } from 'antd';
import {BackwardFilled, LoadingOutlined} from '@ant-design/icons'
import { Link } from "react-router-dom";
import './Button.scss'

const ButtonBack = () => {
    return (
        <div>
            <Link to={"/"}>
            <Button className="button-back"><BackwardFilled /></Button>
            </Link>
        </div>
    )
}

export default ButtonBack
