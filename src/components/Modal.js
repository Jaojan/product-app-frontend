import React, { useState } from 'react'
import { Modal, Button } from 'antd';


const ModalCard = ({isModalVisible, handleOk, handleCancel}) => {
console.log(isModalVisible);
    return (
            <Modal title="คุณได้เพิ่มสินค้า" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                <h1>เพิ่มสินค้าเสร็จสิ้น</h1>
            </Modal>
     
    )
}

export default ModalCard
