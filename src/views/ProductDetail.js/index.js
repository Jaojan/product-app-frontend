import React, { useEffect, useState } from "react";
import { ShoppingCartOutlined } from '@ant-design/icons';
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
// import { selectedProduct } from '../../redux/actions/actions'
import { loadProdcutByID } from '../../redux/api'
import { Card, Modal } from 'antd';
import './index.scss'
import ButtonBack from '../../components/ButtonBack'
import ModalCard from '../../components/Modal'


const { Meta } = Card;
const { confirm } = Modal


const ProductDetailPage = () => {

    const { productID } = useParams();

    //Send action
    const dispatch = useDispatch();
    const [product, setProduct] = useState([])
    const [visible, setVisible] = useState(false)


    //Get id useParam
    console.log(productID);

    //fetch data from api
    const fetchData = async () => {
        const result = await loadProdcutByID(productID)
            .then(response => response)
            .then(data => {
                setProduct(data)
            })
        return result
        // dispatch(loadDataStart())
    }

    useEffect(() => {
        fetchData()
    }, [])

    const showModal = () => {
        console.log('Show');
        setVisible(true)
    }

    const handleOk = e => {
        console.log(e);
        setVisible(false)
    }

    const handleCancel = e => {
        console.log(e);
        setVisible(true)
    };

    return (
        <div className="product-detail">
            <Card
                bordered={true}
                cover={<img alt={product.name} src={product.imageURL} />}
                actions={[
                    <ShoppingCartOutlined key="setting" onClick={showModal} />,
                ]}
            >
                <Meta title={`${product.name}`} description={`รุ่น ${product.description}`} />
                <br />
                <br />
                <Meta title="Price:" description={`${product.price} $`} />

            </Card>
            <div>
                <ButtonBack />
            </div>
            {
                visible &&  <ModalCard isModalVisible={visible} handleOk={handleOk} handleCancel={handleCancel} />
            }
        </div>
    )
}

export default ProductDetailPage
