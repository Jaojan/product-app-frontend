import React, { useEffect, useState } from 'react'
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import './index.scss'
import { useDispatch, useSelector } from 'react-redux'
import { loadDataStart } from '../../redux/actions/actions'
import Products from './ProductList'
import PaginationCard from '../../components/Pagination'



const Product = () => {

    const [loading, setLoading] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [productsPerPage] = useState(9);


    const dispatch = useDispatch();
    //fetch data from api
    const fetchData = () => {
        dispatch(loadDataStart())
    }

    useEffect(() => {
        fetchData()
        chckData()
    }, [])

    const state = useSelector((state) => ({ ...state.app }))

    //check data and set loading
    const chckData = () => {
        if (state.data && !state.loading) {
            setLoading(true)
        }
    }

    // Get current posts
    const indexOfLastPost = currentPage * productsPerPage;
    const indexOfFirstPost = indexOfLastPost - productsPerPage;
    const currentProducts = state.data.slice(indexOfFirstPost, indexOfLastPost);

    // Change page
    const paginate = pageNumber => setCurrentPage(pageNumber);


    return (
        <div className="product-container">
            <Products products={currentProducts} loading={loading} />
            <PaginationCard
                productsPerPage={productsPerPage}
                totalProducts={state.data.length}
                paginate={paginate}
            />
        </div>
    )
}

export default Product
