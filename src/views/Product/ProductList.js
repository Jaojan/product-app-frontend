import React, { useState, useEffect } from 'react';
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import './index.scss'
import { Layout, Card, Spin, Row, Col, Button } from 'antd';
import { SettingOutlined, ShoppingCartOutlined, MoreOutlined, LoadingOutlined } from '@ant-design/icons';
import { Link } from "react-router-dom";

const { Header, Content } = Layout;
const { Meta } = Card;


const Products = ({ products, loading }) => {

    const [visible, setVisible] = useState(4);

    if (!loading) {
        return <LoadingOutlined key="setting" />;
    }

    //showMoreProduct
    const showMoreProduct = () => {
        console.log(visible);
        setVisible(prevValue => prevValue + 4)
    }


    return (
        <div className="product-container">
            <Row>
                {products.length ? products.slice(0, visible).map((data, index) => (
                    <Col span={12} gutter={8}>
                        <Link to={`/product/${data._id}`}>
                            <Card
                                className="card-text"
                                key={index}
                                cover={
                                    <img
                                        alt={data.name}
                                        src={data.imageURL}
                                        className="img-card"
                                    />
                                }
                                actions={[
                                    <ShoppingCartOutlined key="setting" />,
                                ]}
                                bordered={true}
                            >
                                <Meta
                                    title={data.name}
                                    description={data.description}
                                />
                                <br />
                                <Meta
                                    title={`${data.price} $`}
                                />
                            </Card>
                        </Link>
                    </Col>
                )) : null}
            </Row>
            <Button type="danger" onClick={showMoreProduct}><MoreOutlined key="setting" />Load More</Button>
        </div>

    );
};

export default Products;